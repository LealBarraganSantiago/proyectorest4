<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$app->get('/nuveles-economico', function () use ($app){
    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getNivelEconomico();

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'nivel_economico' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "nivel_economico", "id_nivel_economico", $contenido);
    }
});