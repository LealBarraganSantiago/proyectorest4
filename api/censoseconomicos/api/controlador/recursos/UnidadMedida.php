<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$app->get('/unidades-de-medida', function () use ($app){
    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getUnidadMedida();

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'unidades_de_medida' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "unidades-de-medida", "id_unidad_medida", $contenido);
    }
});

$app->get('/municipios/unidades-de-medida/:id', function ($id) use ($app){
    if (!is_numeric($id)) {
        validar_id($app,"unidad_medida");
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getNivelesMedidaDeUnMunicipio($id);
      
        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'unidad_medida' => $contenido
                
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "unidad-medida", "id_unidad_medida", $contenido);
        }
    }
});