<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$app->get('/municipios', function() use ($app) {


    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getMunicipio();

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'municipios' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "muninipio", "id_municipio", $contenido);
    }
});


$app->get('/municipios/:id', function($id) use ($app) {
    if (!is_numeric($id)) {
        validar_id($app,"municipios");
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getMunicipioEspecifico($id);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'municipios' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "municipios", "id_municipio", $contenido);
        }
    }
});


$app->get('/entidades/:id/municipios', function($id) use ($app) {
    if (!is_numeric($id)) {
        validar_id($app,"municipio");
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getMunicipiosDeUnaEntidadEspecifica($id);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            
            $resultado = array(
                'municipios' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "municipios", "id", $contenido);
        }
    }
});


$app->delete('/municipios/:id', function($id) use($app) {
    $db = new Conection();
    
    $headers = $app->request()->headers('Accept');
    if (!is_numeric($id))
        validar_id($app, "municipios");
    else {
        $consulta = $db->val_tabla("municipio","id_municipio", $id);
        if ($headers == 'application/json') {
            $app->response()['Content-Type'] = 'application/json';
            if (count($consulta) >= 1) {
                $db->deleteMunicipio($id);
                
                $app->response()->status(200);
                
                $resultado = array(
                    'mensaje' => "exito en la operacion"
                );
                $app->response()->body(json_encode($resultado));
            } else {
                $resultado = array(
                    'mensaje' => "el id que introduciste no  existe"
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            }
        } else {
            $app->response()['Content-Type'] = 'application/xml';
            if (count($consulta) >= 1) {
                $db->deleteMunicipio($id);
                $resultado = array(
                    'mensaje' => "exito en la operacion"
                );
                $contenido = [$resultado];
                $app->response()->status(200);
                xml_datos($app, "munisipio", "", $contenido);
            } else {
                $resultado = array(
                    'mensaje' => "el id que introduciste no existe"
                );
                $contenido = [$resultado];
                $app->response()->status(400);
                xml_datos($app, "municipio", "", $contenido);
            }
        }
    }
});



$app->post('/entidades/:id/municipios', function($id) use ($app) {
   $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $nueva_entidad = $app->request()->getBody();
    
    if ($app->request()->headers('Content-Type') == 'application/json') {
        if ($headers == 'application/json') {
            $datos = json_decode($nueva_entidad);
            $nombre = $datos->municipio->nombre;
           
            
             
           $consulta = $db->val_tabla("municipio","nombre", $nombre);
            //var_dump($consulta);        
            
            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso"
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {
                
                $db->setPostMunicipio($id,$nombre);
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id.'/municipios', 303);
               $app->response()->status(303);
            }
        } else {
            $datos = json_decode($nueva_entidad);
            $nombre = $datos->municipio->nombre;
            
            
            $consulta = $db->val_tabla("municipio","nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                  $db->setPostMunicipio($id,$nombre);
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id.'/municipios', 303);
               $app->response()->status(303);
               
            }
        }
    } else {
        $nueva_entidad = $app->request()->getBody();
        if ($headers == 'application/json') {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($nueva_entidad);
            
            //var_dump($datos) ;
            $nodo_nombre = $datos->getElementsByTagName('nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;
            
            $consulta = $db->val_entidad("nombre", $nombre);
            //var_dump($consulta);
            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso",
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {

                $db->setPostMunicipio($id,$nombre);
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id.'/municipios', 303);
               $app->response()->status(303);
            }
        } else {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($nueva_entidad);
            $nodo_nombre = $datos->getElementsByTagName('nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;
            

            $consulta = $db->val_entidad("nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "municipio", "", $contenido);
            } else {
                $db->setPostMunicipio($id,$nombre);
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id.'/municipios', 303);
               $app->response()->status(303);
            }
        }
     
    }
     
  
    
});


/*
 * 
$app->get('/temas/:id/subtema1/:id/subtemas2', function($id,$id2) use ($app) {
    if (!is_numeric($id)) {
        validar_id($app,"temas");
        $app->response()->status(400);
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getTemasNivel1DelNivel2Nivel3($id,$id2);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'subtema2' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "subtemas2", "id_subtema2", $contenido);
        }
    }
});
 */