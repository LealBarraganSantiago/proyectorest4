<?php
require 'api/vendor/autoload.php';

/* How to organize a large Slim Framework application
 * http://www.slimframework.com/news/how-to-organize-a-large-slim-framework-application
 */

/*
 * Documentation:
 * http://php.net/manual/en/function.date-default-timezone-set.php
 * http://www.php.net/manual/en/timezones.php
 */


session_cache_limiter(false);
session_start();

//header('Content-Type: application/xml; charset=UTF-8');
date_default_timezone_set('UTC');
$app = new \Slim\Slim();
//$app_response = $app->response()->header("Content-Type", "application/json");
//$app_response=$app->response();
//$app_request = $app->request();

require_once './api/controlador/Conection.php';
//require './api/controlador/validar.php';
//require './api/controlador/consultas.php';
require './api/controlador/validador.php';
require './api/controlador/response.php';
//require 'api/controlador/metodos/get.php';
require 'api/controlador/recursos/Entidades.php';
require 'api/controlador/recursos/Municipios.php';
require 'api/controlador/recursos/NivelEconomico.php';
require 'api/controlador/recursos/UnidadMedida.php';
require 'api/controlador/recursos/Notas.php';
require 'api/controlador/recursos/Indicadores.php';
require 'api/controlador/recursos/temas.php';
require 'api/controlador/recursos/subtenas1.php';
require 'api/controlador/recursos/subtemas2.php';

//require 'api/controlador/recursos/temas.php';
//require 'api/controlador/recursos/subtemas1.php';
//require 'api/controlador/recursos/subtemas2.php';
//require 'api/controlador/metodos/post.php';
//require 'api/controlador/metodos/delete.php';


$app->run();

?>