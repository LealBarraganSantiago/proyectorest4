

function crear_input_text(name,id,value,clas)
{
    obj_tem= document.createElement("input");
    obj_tem.type= "text";
    obj_tem.name = name;
    obj_tem.id = id;
    obj_tem.value = value;
    obj_tem.setAttribute("class",clas);
        //obj_tem.maxLength = max_long;

    return obj_tem;
}

function crear_input_button(name,id,value,clas,conte)
{
    obj_tem= document.createElement("button");
    obj_tem.type= "button";
    obj_tem.name = name;
    obj_tem.id = id;
    obj_tem.value = value;
    obj_tem.setAttribute("class",clas);
    obj_tem.textContent = conte;
    //obj_tem.maxLength = max_long;

    return obj_tem;
}


function crear_input_pass(name,value)
{
    obj_tem= document.createElement("input");
    obj_tem.type= "password";
    obj_tem.name = name;
    obj_tem.value = value;
    return obj_tem;
}
function crear_label (for1,clas,name) {
    // body...
   obj =  document.createElement("label");
    obj.setAttribute("for",for1);
    obj.setAttribute("class",clas);
    obj.textContent = name;

    return obj;

}


function crear_form(name,id,method,clas,rol)
{
    obj_form = document.createElement("form");
    obj_form.name = name;
    obj_form.id = id;
    obj_form.method=method;
    obj_form.setAttribute("class",clas);
    obj_form.setAttribute("role",rol);
    return obj_form;
}


function crear_option(value,id,contenido)
{
    obj_option = document.createElement("option");
    obj_option.value = value;
    obj_option.id = id;
    obj_option.textContent= contenido;
    return obj_option;
}

function crear_select(id,titulo,clas)
{
    obj_select = document.createElement("select");
    obj_select.id= id;
    obj_select.title = titulo;
    obj_select.setAttribute("class",clas);
    return obj_select;
}
function crear_textArea(id,titulo,clas,row)
{
    obj = document.createElement("textarea");
    obj.id= id;
    obj.title = titulo;
    obj.setAttribute("class",clas);
    obj.rows= row;
    
    return obj;
}


function crear_div(clas,id) {
    // body...
    obj_select = document.createElement("div");
    obj_select.setAttribute("id",id);
    obj_select.setAttribute("class",clas);
   return obj_select;
}



function crear_h1(id,conten) {
    // body...
     obj = document.createElement("h1");
    obj.setAttribute("id",id);
    obj.textContent=conten;

    return obj;
}

function crear_span (clas,contenido) {
    // body...

    obj = document.createElement("span");
    obj.setAttribute("class",clas);
    obj.textContent= contenido;
   return obj;
}





//============================================================
//=========== creacion tablas ================================

function crear_tabla(clas,id) {

    // body...
    obj = document.createElement("table");
    obj.setAttribute("class",clas);
    obj.id = id;
    return obj;
}


function crear_thead(clas,id) {

    // body...
    obj = document.createElement("thead");
    obj.setAttribute("class",clas);
    obj.id = id;
    return obj;
}


function crear_tbody(clas,id) {

    // body...
    obj = document.createElement("tbody");
    obj.setAttribute("class",clas);
    obj.id = id;
    return obj;

}
function crear_tr(clas,id) {

    // body...
    obj = document.createElement("tr");
    obj.setAttribute("class",clas);
    obj.id = id;
    return obj;

}

function crear_th(clas,id,conten) {

    // body...
    obj = document.createElement("th");
    obj.setAttribute("class",clas);
    obj.id = id;
    obj.textContent=conten;
    return obj;

}
function crear_td(clas,id,conten) {

    // body...
    obj = document.createElement("td");
    obj.setAttribute("class",clas);
    obj.id = id;
    obj.textContent=conten;
    return obj;

}

//============================================================
//=========== creacion tablas ================================




//crear_div("panel panel-default","lqw")




function nuevo_elemento_body(hijo)
{   
    document.body.appendChild(hijo);

}

function nuevo_elemento_form(padre,hijo)
{   
    padre.appendChild(hijo);

}

function nuevo_elemento(padre,hijo)
{   
    padre.appendChild(hijo);

}


function nuevo_elemento_div(padre,hijo)
{   
    padre.appendChild(hijo);

}


function nuevo_elemento_h1(padre,hijo)
{   
    padre.appendChild(hijo);

}

function nuevo_elemento_select(padre,hijo)
{   
    padre.appendChild(hijo);

}
