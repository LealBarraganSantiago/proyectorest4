--municipio.tema 3,unidad medida,  cantidad(los agrupa por municipio,tema y unidad medida, se especifica la unidad de medida..para graficar datos de un solo tipo)

select  m.nombre as nombreMunicipio,n3.tema_nivel3,u.medida,sum(v.cantidad)
from entidad e
inner join municipio m on  m.id_entidad=e.id_entidad
inner join valor_economico v on m.id_municipio=v.id_municipio
inner join nivel3 n3 on n3.id_nivel2=v.id_nivel3
inner join nivel2 n2 on n2.id_nivel2=n3.id_nivel3
inner join nivel1 n1 on n1.id_nivel1=n2.id_nivel1
inner join unidad_medida u on u.id_unidad_medida=v.id_unidad_medida
inner join indicador i on i.id_indicador=v.id_indicador
 where u.id_unidad_medida=4
group by m.id_municipio,n3.id_nivel3,u.medida
